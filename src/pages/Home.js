import { Fragment } from 'react';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){
	const data = {
		title: "Christopher's E-Commerce",
		content: "Begin your experience by Staking, Exchanging, and Trading",
		destination: "/products",
		label: "Buy Now!"
	}
	return(
		<Fragment>
			<Banner data={data} />
			<Highlights />
		</Fragment>
	)
}