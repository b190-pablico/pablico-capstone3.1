import { Fragment, useEffect, useState, useContext } from 'react';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import UserContext from '../UserContext';

export default function Products() {

    const { user } = useContext(UserContext);

    const [products, setProducts] = useState([]);

    const fetchData = () => {

        fetch(`${ process.env.REACT_APP_API_URL}/products`)
        .then(res => res.json())
        .then(data => {

            setProducts(data);

        });

    }

    useEffect(() => {
        fetchData();
    }, []);
    
    return (
        <Fragment>
            {(user.isAdmin === true)
                ? <AdminView productsData={products} fetchData={fetchData}/>
                : <UserView productsData={products}/>
            }
        </Fragment>
    )

}