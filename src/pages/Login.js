import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Login() {
	const { user, setUser } = useContext(UserContext);

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	const [ isActive, setIsActive] = useState(false);


	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data=>{
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(()=>{
		if (email !== ''&& password !== '') {
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password]);

	function loginUser(e){
		e.preventDefault();

		
		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
			method: 'POST',
			headers:{
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (typeof data.access !== 'undefined') {
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title:"Login Successful!",
					icon: "sucess",
					text: "Welcome to Christopher's E-Commerce!"
				})
			} else{
				Swal.fire({
					title: "Authentication Failed.",
					icon: "error",
					text: "Check your login credentials and try again."
				})
			}
		});

		
		setEmail('');
		setPassword('');
	}


	return(
		(user.id !== null) ?
		<Navigate to='/courses' />
		:
		  <Form onSubmit={(e) => loginUser(e)}>
		    <Form.Group controlId="userEmail">
		      <Form.Label>Email address</Form.Label>
		      <Form.Control 
		      				type="email" 
		      				placeholder="Enter email" 
		      				value={email}
		      				onChange={e => setEmail(e.target.value)}
		      				required 
				/>
		      <Form.Text className="text-muted">
		        We'll never share your email with anyone else.
		      </Form.Text>
		    </Form.Group>

		    <Form.Group controlId="password1">
		      <Form.Label>Password</Form.Label>
		      <Form.Control
		      				type="password" 
		      				placeholder="Password" 
		      				value={password}
		      				onChange={e => setPassword(e.target.value)}
		      				required
				/>
		    </Form.Group>

		    {isActive ?
		    <Button variant="primary" type="submit" id="submitBtn">Login</Button>
		    :
		    <Button variant="primary" type="submit" id="submitBtn" disabled>Login</Button>
			}
		  </Form>
		);
}