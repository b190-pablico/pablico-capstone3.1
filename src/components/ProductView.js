import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function ProductView(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const { productId } = useParams();

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);

	const enroll = (productId) => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/enroll`, {
			method: "POST",
			headers: {
				'Content-type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data);

			if (data === true) {
				Swal.fire({
					title: "Successfully Enrolled",
					icon: "success",
					text: "Added to cart"
				});
				navigate('/products');
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(()=>{
		console.log(productId);

		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
		.then(res => res.json())
		.then(data=>{
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	},[productId]);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>

							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price</Card.Subtitle>
							<Card.Text>{price}</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>8 am - 5 pm</Card.Text>

							{ (user.id !== null)?
							<Button variant="primary" onClick={()=>enroll(productId)} block>Enroll</Button>
							:
							<Link className='btn btn-danger btn-block' to='/login'>Login to enroll</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)



}